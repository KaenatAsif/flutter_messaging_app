import 'package:flutter/material.dart';

void main() {
  runApp(new FriendlychatApp());
}
const String _name = "Kaenat";

class FriendlychatApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: "Friendly chat",
      home: new ChatScreen(),
    );
  }
}

class ChatScreen extends StatefulWidget {
  @override
 State createState()=> new ChatScreenState();


}

class ChatScreenState extends State<ChatScreen> with TickerProviderStateMixin{
  bool _isComposing = false;
  @override
  Widget build(BuildContext context) {
   return new Scaffold(
     appBar: new AppBar(title: new Text("Friendly Chat")),
     body: new Column(                                        //modified
       children: <Widget>[                                         //new
         new Flexible(                                             //new
           child: new ListView.builder(                            //new
             padding: new EdgeInsets.all(8.0),                     //new
             reverse: true,                                        //new
             itemBuilder: (_, int index) => _messages[index],      //new
             itemCount: _messages.length,                          //new
           ),                                                      //new
         ),                                                        //new
         new Divider(height: 1.0),                                 //new
         new Container(                                            //new
           decoration: new BoxDecoration(
               color: Theme.of(context).cardColor),                  //new
           child: _buildTextComposer(),                       //modified
         ),                                                        //new
       ],                                                          //new
     ),                                                            //new
   );
  }
  final TextEditingController _textEditingController= new TextEditingController();
  Widget _buildTextComposer(){
    return new IconTheme(
        data: new IconThemeData(color: Theme.of(context).accentColor), //new
        child: new Container(
          margin: const EdgeInsets.symmetric(horizontal: 10.0),
          child: new Row(                                            //new
            children: <Widget>[                                      //new
             new Flexible(                                          //new
               child: new TextField(
                 controller: _textEditingController,
                 onChanged: (String text) {          //new
                   setState(() {                     //new
                     _isComposing = text.length > 0; //new
                   });                               //new
                 },                                  //new
                 onSubmitted: _handleSubmitted,
                 decoration: new InputDecoration.collapsed(
                 hintText:"Send a Message"),
              ),
             ),
            new Container(                                                 //new
              margin: new EdgeInsets.symmetric(horizontal: 4.0),           //new
              child: new IconButton(                                       //new
                  icon: new Icon(Icons.send),                                //new
                  onPressed: () => _handleSubmitted(_textEditingController.text)),  //new
            ),
          ],
     ),
        ),
    );
  }

  void _handleSubmitted(String text) {
    _textController.clear();
    setState(() {                                                    //new
      _isComposing = false;                                          //new
    });                                                              //new
    ChatMessage message = new ChatMessage(                         //new
      text: text,                                                  //new
      animationController: new AnimationController(                  //new
        duration: new Duration(milliseconds: 700),                   //new
        vsync: this,                                                 //new
      ),                                                             //new
    );                                                               //new
    setState(() {
      _messages.insert(0, message);
    });
    message.animationController.forward();                           //new
  }
  @override
  void dispose() {                                                   //new
    for (ChatMessage message in _messages)                           //new
      message.animationController.dispose();                         //new
    super.dispose();                                                 //new
  }

  final List<ChatMessage> _messages = <ChatMessage>[];             // new
  final TextEditingController _textController = new TextEditingController();
}
  class ChatMessage extends StatelessWidget {

    final String text;
    final AnimationController animationController;
    ChatMessage({
      this.text, this.animationController
    });
    @override
    Widget build(BuildContext context) {
      return new SizeTransition(
          sizeFactor: new CurvedAnimation(                              //new
            parent: animationController, curve: Curves.easeOut),      //new
      axisAlignment: 0.0,                                           //new
      child: new Container(                                    //modified
          margin: const EdgeInsets.symmetric(vertical: 10.0),
          child: new Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              new Container(
                margin: const EdgeInsets.only(right: 16.0),
                child: new CircleAvatar(child: new Text(_name[0])),
              ),
              new Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  new Text(_name, style: Theme.of(context).textTheme.subhead),
                  new Container(
                    margin: const EdgeInsets.only(top: 5.0),
                    child: new Text(text),
                  ),
                ],
              ),
            ],
          ),
      )
        );
    }
  }

